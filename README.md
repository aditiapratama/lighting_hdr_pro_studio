# HDR-ProStudio

[![HDR ProStudio for Blender 2.8](http://img.youtube.com/vi/pl9qyydKiEk/0.jpg)](https://www.youtube.com/watch?v=pl9qyydKiEk "HDR ProStudio for Blender 2.8")

HDR-ProStudio is a Blender addon that helps you produce eye-candy results, with thousands of possible lighting solutions for your product, character or other 3D art. With HDR-ProStudio, you’ll discover new lighting setups, and create a gorgeous render in minutes.

Features:

    Procedural Lights: Ellipse and Polygonal
    Image Lights
    Unlimited Lights
    Supports native paint tools painting on the HDRI.
    Independent light bulb settings with brightness curve editing.
    Flat, Gradient(linear,radial and conical falloffs) and image textures.
    Multiple Blend Modes supported to mix lights and images together
    Real time updates in preview render(Cycles Render)
    Export the 32bit image as HDR or OpenEXR

New changes for Blender 2.8:

    Works with Eevee!
    Multithreaded and faster overall due to major code rewrite and cleanup.
    New border previews for faster operations when moving lights.
    Max Brightness is now 100. We included soft values too.
    Scale and rotate limitations are now gone.
    Imported images can be used as lights, not just as a background.
    Append HDR images with lights from another blend file.
    Added icons instead of ugly checkboxes.
    Reorder lights in layer list.
    Several bug fixes.
